package com.achmad_theo4210171013.tugasminggu5;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class RegisterPageActivity extends AppCompatActivity implements View.OnClickListener {

    Button backBtn;
    Button registerBtn;

    EditText emailText;

    public static String EMAIL_TEXT = "default";
    public static int RESULT_CODE = 110;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_page);

        backBtn = findViewById(R.id.backBtn);
        backBtn.setOnClickListener(this);

        registerBtn = findViewById(R.id.account_button);
        registerBtn.setOnClickListener(this);

        emailText = findViewById(R.id.email);
    }

    @Override
    public void onClick(View v){
        switch (v.getId()) {
            case R.id.backBtn:
                Intent moveIntent = new Intent(RegisterPageActivity.this, MainActivity.class);
                startActivity(moveIntent);
                break;
            case R.id.account_button:
                String textVal = emailText.getText().toString();
                if (textVal != null) {
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra(EMAIL_TEXT, textVal);
                    setResult(RESULT_CODE, resultIntent);
                    finish();
                }
                break;
        }
    }

}